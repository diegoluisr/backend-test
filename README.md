# Proyecto Backend

Este proyecto ha sido realizado para demostrar mis capacidades en el desarrollo de aplicaciones Backend RestFul. En este proyecto se usa:

* [SlimFramework](https://www.slimframework.com/)
* [PropelORM](http://propelorm.org/)
* [Composer](https://getcomposer.org/)

Tambien se usan algunas librerias importantes para la gestión de los accesos, permisos, internacionalización, etc. como:

* [JSON Web Tokens](https://jwt.io/)
* [PSR-7 JWT Authentication Middleware](https://github.com/tuupola/slim-jwt-auth)
* [Access Control List](https://github.com/alexshelkov/SimpleAcl)

Este repositorio es un proyecto en Vagrant que sirve como ambiente de trabajo común

## Requerimientos

* Clonar este repositorio en su maquina local `git clone git@bitbucket.org:diegoluisr/backend-test.git`
* Descargar e instalar [VirtualBox](https://www.virtualbox.org/)
* Descargar e instalar [Vagrant](https://www.vagrantup.com/)
* Tener 2Gb de Memoria libre en el equipo
* Tener conocimientos básicos en el uso de CMD o Terminal 

Una vez tenga instalado todos los requerimiento ubíquese en el directorio raíz del repositorio clonado usando la terminal y allí ingrese el comando `vagrant up`, el cual pondrá a correr una maquina virtual y realizara el aprovisionamiento y configuración necesario para que el proyecto funcione.

Si desea correr las pruebas unitarias ingrese a la maquina virtual usando el comando `vagrant ssh`, una vez dentro ubiquese en la carpeta notes `cd /vagrant/notes` y allí use el comando `sh unittest.sh` para evaluar toda la suite. Por le momento solo he creado algunas pruebas para Login, Register y Categories.

## Licencia
Este repositorio se ofrece a la comunidad bajo licencia MIT, exceptuando los componentes que tengan licenciamiento diferente.