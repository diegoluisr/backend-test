#!/usr/bin/env bash

DBNAME=api_db
DBUSER=root
DBPASSWD=toor
DBSCHEMAEXPORT=/vagrant/www/html/api/App/Config/propel/default.sql
DBDATAEXPORT=/vagrant/www/html/api/App/Config/propel/data.sql

cd /vagrant/www/html/api

# EXECUTE FOR MODEL BUILD 
vendor/propel/propel/bin/propel model:build --config-dir="App/Config/propel" --schema-dir="App/Config/propel"

# EXECUTE FOR SQL BUILD
vendor/propel/propel/bin/propel sql:build --config-dir="App/Config/propel" --output-dir="App/Config/propel" --overwrite

# mysql -u root -ptoor api_db < App/Config/propel/default.sql
# mysql -u root -ptoor api_db < App/Config/propel/data.sql

mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBSCHEMAEXPORT  > /dev/null 2>&1
mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBDATAEXPORT  > /dev/null 2>&1