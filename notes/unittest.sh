#!/usr/bin/env bash

DBNAME=api_db
DBUSER=root
DBPASSWD=toor
DBSCHEMAEXPORT=/vagrant/www/html/api/App/Config/propel/default.sql
DBDATAEXPORT=/vagrant/www/html/api/App/Config/propel/data.sql

# Init database
mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBSCHEMAEXPORT > /dev/null 2>&1
mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBDATAEXPORT > /dev/null 2>&1

cd /vagrant/www/html/api/tests
phpunit --colors=always