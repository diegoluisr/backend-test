<?php

namespace Models;

use Models\Base\ProductsCategoriesQuery as BaseProductsCategoriesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'products_categories' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProductsCategoriesQuery extends BaseProductsCategoriesQuery
{

}
