<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class UsersRolesController extends AppController {

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        // $jwt = $request->getAttribute("jwt");

        if($payload) {

            $user_id = isset($payload['UserId']) ? $payload['UserId'] : null;
            $role_id = isset($payload['RoleId']) ? $payload['RoleId'] : null;

            $user = \Models\UserQuery::create()
                ->findPK($user_id);
            $role = \Models\RoleQuery::create()
                ->findPK($role_id);

            if(is_object($user) && is_object($role)) {
                $user_role = new \Models\UsersRoles();
                $user_role->setUser($user);
                $user_role->setRole($role);

                $user_role->save();
                $data = $user_role->toArray();
                $this->result['data'] = $data;
                return $response->withJson($this->result)
                    ->withStatus(201);
            } else {
                $this->result['message'] = 'Related data is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Invalid payload!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
        return $response->withJson($this->result);
    }
}