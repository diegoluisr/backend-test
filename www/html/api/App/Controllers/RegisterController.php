<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;

class RegisterController extends AppController {

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = 'Invalid payload!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $name = isset($payload['Name']) ? $payload['Name'] : null;
        $email = isset($payload['Email']) ? filter_var($payload['Email'], FILTER_VALIDATE_EMAIL) : null;
        $password = isset($payload['Password']) ? $payload['Password'] : null;

        $role = \Models\RoleQuery::create()->findPK(DEFAULT_USER_ROLE_ID); // Guest

        $user = new \Models\User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword(Utils::secure($password));
        $user->addRole($role);

        if (!$user->validate()) {
            $this->result['message'] = 'Validation error!';
            foreach ($user->getValidationFailures() as $failure) {
                $this->result['fields'][] = [
                    "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                    "message" => $failure->getMessage()
                ];
            }
            return $response
                ->withJson($this->result)
                ->withStatus(400);
        } else {
            $user->save();
            $this->result['data'] = $user->toArray();
            // PROBLEM WITH RECURSION
            // $this->result['data']['Roles'] = $user->getRoles()->toArray();
            return $response
                ->withJson($this->result)
                ->withStatus(201);
        }
    }
}
