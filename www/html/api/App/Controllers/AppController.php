<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AppController {

    protected $result;

    /**
     * Contructor
     * @param Slim $app objeto aplicacion
     */

    function __construct(){
        $this->result = [];
    }


    /**
     * funcion enlazada con el metodo http GET
     * @return string 
     */

    public function index(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }


    /**
     * funcion enlazada con el metodo http GET
     * @param int $id identificador del registro
     * @return string 
     */

    public function view(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }


    /**
     * funcion enlazada con el metodo http POST
     * @return string 
     */

    public function add(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }


    /**
     * funcion enlazada con el metodo http PUT
     * @param int $id identificador del registro
     * @return string 
     */
    public function edit(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }

    /**
     * funcion enlazada con el metodo http PUT
     * @param int $id identificador del registro
     * @return string 
     */
    public function patch(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }

    /**
     * funcion enlazada con el metodo http DELETE
     * @param int $id identificador del registro
     * @return string 
     */
    public function delete(Request $request, Response $response, $args) {
        return $response->withJson($this->result);
    }
}