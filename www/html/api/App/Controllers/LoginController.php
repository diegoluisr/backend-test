<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;
use \Firebase\JWT\JWT;

class LoginController extends AppController{

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
    	$payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = "Some fields are required!";
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

    	$email = isset($payload['Email']) ? filter_var($payload['Email'], FILTER_VALIDATE_EMAIL) : null;
        $password = isset($payload['Password']) ? $payload['Password'] : null;

        // print_r(Utils::secure($password));
        $user = \Models\UserQuery::create()
            ->filterByEmail($email)
            ->filterByPassword(Utils::secure($password))
            ->findOne();

        if(!is_object($user)){
            $this->result['message'] = 'Email or password not valid!';
            return $response
                ->withJson($this->result)
                ->withStatus(400);
        }

        $data = array(
            'iat' => time(),
            'nbf' => time(),
            'uid' => $user->getId(),
            'roles' => []
        );

        foreach ($user->getRoles() as $role) {
            $data['roles'][] = $role->getName();
        }

        $jwt = JWT::encode($data, JWT_SECRET);

        // https://tools.ietf.org/html/rfc6750
        $this->result['data'] = [
            'header' => 'Authorization',
            'type' => 'Bearer',
            'credentials' => $jwt
        ];

        return $response->withJson($this->result)
            ->withStatus(200);
    }
}