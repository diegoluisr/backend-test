<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;

class CategoriesController extends AppController{
    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = "Some fields are required!";
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $name = isset($payload['Name']) ? $payload['Name'] : null;

        $category = new \Models\Category();
        $category->setName($name);

        if (!$category->validate()) {
            $this->result['message'] = 'Validation error!';
            foreach ($category->getValidationFailures() as $failure) {
                $this->result['fields'][] = [
                    "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                    "message" => $failure->getMessage()
                ];
            }
            return $response
                ->withJson($this->result)
                ->withStatus(400);
        } else {
            $category->save();
            $this->result['data'] = $category->toArray();
            return $response
                ->withJson($this->result)
                ->withStatus(201);
        }
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            $categories = \Utils\SearchFilter::filter($request, 'Category', [
                'SearchFields' => ['Name'],
                'OrderFields' => ['Id', 'Email']
            ]);
            if(is_object($categories)) {
                $this->result['data'] = $categories->toArray();
            } else {
                $this->result['message'] = 'Empty response!';
            }
            return $response->withJson($this->result);
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
            if(!is_int($id)) {
                $this->result['message'] = 'Item "Id" is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }

            $category = \Models\CategoryQuery::create()->findPk($id);
            if(!is_object($category)) {
                $this->result['message'] = 'Item does not exists!';
                return $response
                    ->withJson($this->result)
                    ->withStatus(404);
            }

            $this->result['data'] = $category->toArray();
            return $response->withJson($this->result);
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function edit(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Item "Id" is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = 'Invalid payload!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $name = isset($payload['Name']) ? $payload['Name'] : null;

        $category = \Models\CategoryQuery::create()->findPk($id);
        if(!is_object($category)) {
            $this->result['message'] = 'Item does not exists!';
            return $response
                ->withJson($this->result)
                ->withStatus(404);
        }

        $category->setName($name);
        if (!$category->validate()) {
            $this->result['message'] = 'Validation error!';
            foreach ($category->getValidationFailures() as $failure) {
                $this->result['fields'][] = [
                    "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                    "message" => $failure->getMessage()
                ];
            }
            return $response->withJson($this->result)
                ->withStatus(400);
        } else {
            $category->save();
            $this->result['data'] = $category->toArray();
            return $response
                ->withJson($this->result);
        }
    }

    public function delete(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Item "Id" is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $category = \Models\CategoryQuery::create()->findPk($id);
        if(!is_object($category)) {
            $this->result['message'] = 'Item does not exists!';
            return $response
                ->withJson($this->result)
                ->withStatus(404);
        }

        $category->delete();
        if($category->isDeleted()){
            $this->result['message'] = 'Item deleted successfully!';
            return $response
                ->withHeader("ACL", "REVOKE")
                ->withJson($this->result);
        } else {
            $this->result['message'] = 'Item was not deleted!';
            return $response
                ->withJson($this->result)
                ->withStatus(422);
        }
    }
}