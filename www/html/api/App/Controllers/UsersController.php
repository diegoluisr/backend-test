<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;

class UsersController extends AppController{

    function __construct(){
        parent::__construct();
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            $users = \Utils\SearchFilter::filter($request, 'User', [
                'SearchFields' => ['Email', 'Name'],
                'OrderFields' => ['Id', 'Email', 'Name']
            ]);

            if(is_object($users)) {
                $data = [];
                foreach ($users as $user) {
                    $item = $user->toArray();
                    $item['Roles'] = $user->getRoles()->toArray();
                    $data[] = $item;
                }
                $this->result['data'] = $data;
            } else {
                $this->result['message'] = 'Empty response!';
            }
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? $params[1] : null;
            if(intval($id)) {
                $user = \Models\UserQuery::create()->findPk($id);
                if(is_object($user)) {
                    $data = $user->toArray();
                    $data['Roles'] = $user->getRoles()->toArray();
                    $this->result['data'] = $data;
                    return $response->withJson($this->result);
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Id is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }

    public function add(Request $request, Response $response, $args) {
        // $payload = $request->getParsedBody();

        // if($payload) {
        //     $name = isset($payload['Name']) ? $payload['Name'] : null;
        //     $email = isset($payload['Email']) ? $payload['Email'] : null;
        //     $password = isset($payload['Password']) ? $payload['Password'] : null;

        //     $role = \Models\RoleQuery::create()->findPK(DEFAULT_USER_ROLE_ID); // Guest

        //     $user = new \Models\User();
        //     $user->setName($name);
        //     $user->setEmail($email);
        //     $user->setPassword(Utils::secure($password));
        //     $user->addRole($role);

        //     if (!$user->validate()) {
        //         $this->result['message'] = 'Validation error!';
        //         foreach ($user->getValidationFailures() as $failure) {
        //             $this->result['fields'][] = [
        //                 "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
        //                 "message" => $failure->getMessage()
        //             ];
        //         }
        //         return $response->withJson($this->result)
        //             ->withStatus(400);
        //     } else {
        //         $user->save();
        //         $data = $user->toArray();
        //         $this->result['data'] = $data;
        //         return $response->withJson($this->result)
        //             ->withStatus(201);
        //     }
        // } else {
        //     $this->result['message'] = 'Invalid payload!';
        //     return $response->withJson($this->result)
        //         ->withStatus(412);
        // }
        return $response->withJson($this->result);
    }

    public function edit(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? $params[1] : null;
        if(intval($id)) {
            $payload = $request->getParsedBody();
            if($payload) {
                $name = isset($payload['Name']) ? $payload['Name'] : null;

                $user = \Models\UserQuery::create()->findPk($id);
                if(is_object($user)) {
                    $user->setName($name);

                    if (!$user->validate()) {
                        $this->result['message'] = 'Validation error!';
                        foreach ($user->getValidationFailures() as $failure) {
                            $this->result['fields'][] = [
                                "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                                "message" => $failure->getMessage()
                            ];
                        }
                        return $response->withJson($this->result)
                            ->withStatus(400);
                    } else {
                        $user->save();
                        $this->result['data'] = $user->toArray();
                        return $response->withJson($this->result);
                    }
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Invalid payload!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Item "Id" is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function delete(Request $request, Response $response, $args) {

        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? $params[1] : null;
        if(intval($id)) {
            $user = \Models\UserQuery::create()->findPk($id);
            if(is_object($user)) {
                $user->delete();
                if($user->isDeleted()){
                    $this->result['message'] = 'Item deleted successfully!';
                    return $response->withJson($this->result)
                        ->withStatus(200);
                } else {
                    $this->result['message'] = 'Item was not deleted!';
                    return $response->withJson($this->result)
                        ->withStatus(422);
                }
            } else {
                $this->result['message'] = 'Item does not exists!';
                return $response->withJson($this->result)
                    ->withStatus(404);
            }
        } else {
            $this->result['message'] = 'Item "Id" is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }
}