<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ResourcesController extends AppController {

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        // $jwt = $request->getAttribute("jwt");

        if($payload) {
            $path = isset($payload['Path']) ? $payload['Path'] : null;

            if($path != null) {
                $resource = new \Models\Resource();
                $resource->setPath($path);

                if (!$resource->validate()) {
                    return $response->withJson($this->result)
                        ->withStatus(400);
                } else {
                    $resource->save();
                    $data = $resource->toArray();
                    $this->result['data'] = $data;
                    return $response->withJson($this->result)
                        ->withStatus(201);
                }
            } else {
                $this->result['message'] = 'Some data is required!';
                return $response->withJson($this->result)
                    ->withStatus(201);
            }
        } else {
            $this->result['message'] = 'Invalid payload!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
        return $response->withJson($this->result);
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            $resources = \Models\ResourceQuery::create()->find();
            if(is_object($resources)) {
                $this->result['data'] = $resources->toArray();
            } else {
                $this->result['message'] = 'Empty response!';
            }
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? $params[1] : null;
            if(intval($id)) {
                $resource = \Models\ResourceQuery::create()->findPk($id);
                if(is_object($resource)) {
                    $this->result['data'] = $resource->toArray();
                    return $response->withJson($this->result);
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Id is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }

    public function edit(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();

        if($payload) {
            $path = isset($payload['Path']) ? $payload['Path'] : null;
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? $params[1] : null;
            if(intval($id) && $path != null) {
                $resource = \Models\ResourceQuery::create()->findPk($id);
                if(is_object($resource)) {
                    $resource->setPath($path);
                    if (!$resource->validate()) {
                        $validation = array();
                        foreach ($resource->getValidationFailures() as $failure) {
                            $validation[] = array('field'=>$failure->getPropertyPath(), 'message' => $failure->getMessage());
                        }
                        $this->result['validation'] = $validation;
                        $this->result['message'] = 'validation error';

                        return $response->withJson($this->result)
                            ->withStatus(400);
                    } else {
                        $resource->save();
                        $this->result['data'] = $resource->toArray();
                        return $response->withJson($this->result);
                    }
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Item "Id" is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Invalid payload!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function delete(Request $request, Response $response, $args) {

        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? $params[1] : null;
        if(intval($id)) {
            $resource = \Models\ResourceQuery::create()->findPk($id);
            if(is_object($resource)) {
                $resource->delete();
                if($resource->isDeleted()){
                    $this->result['message'] = 'Item deleted successfully!';
                    return $response->withJson($this->result)
                        ->withStatus(200);
                } else {
                    $this->result['message'] = 'Item was not deleted!';
                    return $response->withJson($this->result)
                        ->withStatus(422);
                }
            } else {
                $this->result['message'] = 'Item does not exists!';
                return $response->withJson($this->result)
                    ->withStatus(404);
            }
        } else {
            $this->result['message'] = 'Item "Id" is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }
}