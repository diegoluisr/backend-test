<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;
// use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Formatter\OnDemandFormatter;

class ProductsController extends AppController{
    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = "Some fields are required!";
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $name =          isset($payload['Name']) ? $payload['Name'] : null;
        $description =   isset($payload['Description']) ? $payload['Description'] : null;
        $image =         isset($payload['Image']) ? filter_var($payload['Image'], FILTER_VALIDATE_URL) : null;
        $is_available =  isset($payload['IsAvailable']) ? filter_var($payload['IsAvailable'], FILTER_VALIDATE_BOOLEAN) : null;
        $is_best_seller = isset($payload['IsBestSeller']) ? filter_var($payload['IsBestSeller'], FILTER_VALIDATE_BOOLEAN) : null;
        $price =         isset($payload['Price']) ? filter_var($payload['Price'], FILTER_VALIDATE_INT) : null;
        $categories    = isset($payload['Categories']) ? $payload['Categories'] : null;

        $product = new \Models\Product();
        $product->setName($name);
        $product->setDescription($description);
        $product->setImage($image);
        $product->setIsAvailable($is_available);
        $product->setIsBestSeller($is_best_seller);
        $product->setPrice($price);

        if(is_array($categories)) {
            foreach ($categories as $category) {
                if(isset($category['Id']) && filter_var($category['Id'], FILTER_VALIDATE_INT)) {
                    $object = \Models\CategoryQuery::create()->findPK($category['Id']);
                    // var_dump($object);
                    if(is_object($object)) {
                        $product->addCategory($object);
                    }
                }
            }
        }

        if (!$product->validate()) {
            $this->result['message'] = 'Validation error!';
            foreach ($product->getValidationFailures() as $failure) {
                $this->result['fields'][] = [
                    "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                    "message" => $failure->getMessage()
                ];
            }
            return $response
                ->withJson($this->result)
                ->withStatus(400);
        } else {
            $product->save();
            $this->result['data'] = $product->toArray();
            // PROBLEM WITH RECURSION
            // $this->result['data']['Categories'] = $product->getCategories()->setFormatter(new OnDemandFormatter())->toArray();
            // $this->result['data']['Categories'] = $product->getCategories()->setFormatter('Propel\Runtime\Formatter\OnDemandFormatter')->toArray();
            return $response
                ->withJson($this->result)
                ->withStatus(201);
        }
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            $products = \Utils\SearchFilter::filter($request, 'Product', [
                'SearchFields' => ['Name', 'Description'],
                'OrderFields' => ['Id', 'Name', 'Price', 'IsAvailable', 'IsBestSeller']
            ]);

            if(is_object($products)) {
                $this->result['data'] = $products->toArray();
            } else {
                $this->result['message'] = 'Empty response!';
            }
            return $response->withJson($this->result);
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
            if(!is_int($id)) {
                $this->result['message'] = 'Item "Id" is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }

            $product = \Models\ProductQuery::create()->findPk($id);
            if(!is_object($product)) {
                $this->result['message'] = 'Item does not exists!';
                return $response
                    ->withJson($this->result)
                    ->withStatus(404);
            }

            $this->result['data'] = $product->toArray();
            return $response->withJson($this->result);
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function edit(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Item "Id" is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = 'Invalid payload!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $name = isset($payload['Name']) ? $payload['Name'] : null;

        $product = \Models\ProductQuery::create()->findPk($id);
        if(!is_object($product)) {
            $this->result['message'] = 'Item does not exists!';
            return $response
                ->withJson($this->result)
                ->withStatus(404);
        }

        $product->setName($name);
        if (!$product->validate()) {
            $this->result['message'] = 'Validation error!';
            foreach ($product->getValidationFailures() as $failure) {
                $this->result['fields'][] = [
                    "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                    "message" => $failure->getMessage()
                ];
            }
            return $response->withJson($this->result)
                ->withStatus(400);
        } else {
            $product->save();
            $this->result['data'] = $product->toArray();
            return $response
                ->withJson($this->result);
        }
    }

    public function delete(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Item "Id" is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $product = \Models\ProductQuery::create()->findPk($id);
        if(!is_object($product)) {
            $this->result['message'] = 'Item does not exists!';
            return $response
                ->withJson($this->result)
                ->withStatus(404);
        }

        $product->delete();
        if($product->isDeleted()){
            $this->result['message'] = 'Item deleted successfully!';
            return $response
                ->withHeader("ACL", "REVOKE")
                ->withJson($this->result);
        } else {
            $this->result['message'] = 'Item was not deleted!';
            return $response
                ->withJson($this->result)
                ->withStatus(422);
        }
    }
}