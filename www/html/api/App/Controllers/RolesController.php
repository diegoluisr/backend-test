<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils as Utils;

class RolesController extends AppController{

    function __construct(){
        parent::__construct();
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            // $roles = \Models\RoleQuery::create()
            //     // ->joinWith('Role.User');
            //     ->find();
            $roles = \Utils\SearchFilter::filter($request, 'Role', [
                'SearchFields' => ['Name'],
                'OrderFields' => ['Id', 'Name']
            ]);
            if(is_object($roles)) {
                $this->result['data'] = $roles->toArray();
            } else {
                $this->result['message'] = 'Empty response!';
            }
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? $params[1] : null;
            if(intval($id)) {
                $role = \Models\RoleQuery::create()->findPk($id);
                if(is_object($role)) {
                    $this->result['data'] = $role->toArray();
                    return $response->withJson($this->result);
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Id is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();

        if($payload) {
            $name = isset($payload['Name']) ? $payload['Name'] : null;

            $role = new \Models\Role();
            $role->setName($name);

            if (!$role->validate()) {
                $this->result['message'] = 'Validation error!';
                foreach ($role->getValidationFailures() as $failure) {
                    $this->result['fields'][] = [
                        "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                        "message" => $failure->getMessage()
                    ];
                }
                return $response->withJson($this->result)
                    ->withStatus(400);
            } else {
                $role->save();
                $data = $role->toArray();
                $this->result['data'] = $data;
                return $response->withJson($this->result)
                    ->withStatus(201);
            }
        } else {
            $this->result['message'] = 'Invalid payload!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function edit(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? $params[1] : null;
        if(intval($id)) {
            $payload = $request->getParsedBody();
            if($payload) {
                $name = isset($payload['Name']) ? $payload['Name'] : null;

                $role = \Models\RoleQuery::create()->findPk($id);
                if(is_object($role)) {
                    $role->setName($name);

                    if (!$role->validate()) {
                        $this->result['message'] = 'Validation error!';
                        foreach ($role->getValidationFailures() as $failure) {
                            $this->result['fields'][] = [
                                "property" => \Utils\Utils::Slug2CamelCase($failure->getPropertyPath()),
                                "message" => $failure->getMessage()
                            ];
                        }
                        return $response->withJson($this->result)
                            ->withStatus(400);
                    } else {
                        $role->save();
                        $this->result['data'] = $role->toArray();
                        return $response->withJson($this->result);
                    }
                } else {
                    $this->result['message'] = 'Item does not exists!';
                    return $response->withJson($this->result)
                        ->withStatus(404);
                }
            } else {
                $this->result['message'] = 'Invalid payload!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }
        } else {
            $this->result['message'] = 'Item "Id" is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function delete(Request $request, Response $response, $args) {

        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? $params[1] : null;
        if(intval($id)) {
            $role = \Models\RoleQuery::create()->findPk($id);
            if(is_object($role)) {
                $role->delete();
                if($role->isDeleted()){
                    $this->result['message'] = 'Item deleted successfully!';
                    return $response->withJson($this->result)
                        ->withStatus(200);
                } else {
                    $this->result['message'] = 'Item was not deleted!';
                    return $response->withJson($this->result)
                        ->withStatus(422);
                }
            } else {
                $this->result['message'] = 'Item does not exists!';
                return $response->withJson($this->result)
                    ->withStatus(404);
            }
        } else {
            $this->result['message'] = 'Item "Id" is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        return $response->withJson($this->result);
    }
}