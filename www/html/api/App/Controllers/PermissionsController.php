<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class PermissionsController extends AppController {

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $payload = $request->getParsedBody();
        // $jwt = $request->getAttribute("jwt");

        if(!is_array($payload)) {
            $this->result['message'] = 'Invalid payload!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $role_id = isset($payload['RoleId']) ? filter_var($payload['RoleId'], FILTER_VALIDATE_INT) : null;
        $resource_id = isset($payload['ResourceId']) ? filter_var($payload['ResourceId'], FILTER_VALIDATE_INT) : null;
        $method = isset($payload['Method']) ? $payload['Method'] : null;

        if($role_id == null || $resource_id == null || $method == null) {
            $this->result['message'] = 'Some data is required!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }

        $role = \Models\RoleQuery::create()
            ->findPK($role_id);
        $resource = \Models\ResourceQuery::create()
            ->findPK($resource_id);

        if(!is_object($role) || !is_object($resource)) {
            $this->result['message'] = 'Related data is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $permission = new \Models\Permission();
        $permission->setRole($role);
        $permission->setResource($resource);
        $permission->setMethod($method);

        if (!$permission->validate()) {
            return $response
                ->withJson($this->result)
                ->withStatus(400);
        } else {
            $permission->save();
            $data = $permission->toArray();
            $this->result['data'] = $data;
            return $response->withHeader("ACL", "REVOKE")
                ->withJson($this->result)
                ->withStatus(201);
        }
        return $response->withJson($this->result);
    }

    public function index(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));

        if(count($params) == 1) {
            $permissions = \Models\PermissionQuery::create()->find();
            if(is_object($permissions)) {
                $this->result['data'] = $permissions->toArray();
            } else {
                $this->result['message'] = 'Empty response!';
            }
            return $response->withJson($this->result);
        } else if(count($params) == 2) {
            $params = explode('/', $request->getAttribute('params'));
            $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
            if(!is_int($id)) {
                $this->result['message'] = 'Item "Id" is required!';
                return $response->withJson($this->result)
                    ->withStatus(412);
            }

            $permission = \Models\PermissionQuery::create()->findPk($id);
            if(!is_object($permission)) {
                $this->result['message'] = 'Item does not exists!';
                return $response
                    ->withJson($this->result)
                    ->withStatus(404);
            }

            $this->result['data'] = $permission->toArray();
            return $response->withJson($this->result);
        } else {
            $this->result['message'] = 'Too much arguments!';
            return $response->withJson($this->result)
                ->withStatus(412);
        }
    }

    public function edit(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Id is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $payload = $request->getParsedBody();
        if(!is_array($payload)) {
            $this->result['message'] = 'Invalid payload!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $role_id = isset($payload['RoleId']) ? filter_var($payload['RoleId'], FILTER_VALIDATE_INT) : null;
        $resource_id = isset($payload['ResourceId']) ? filter_var($payload['ResourceId'], FILTER_VALIDATE_INT) : null;
        $method = isset($payload['Method']) ? $payload['Method'] : null;

        if($role_id == null || $resource_id == null || $method != null) {
            $this->result['message'] = 'Related data is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $role = \Models\RoleQuery::create()
            ->findPK($role_id);
        $resource = \Models\ResourceQuery::create()
            ->findPK($resource_id);

        $permission = \Models\PermissionQuery::create()->findPk($id);
        if(!is_object($permission)) {
            $this->result['message'] = 'Item does not exists!';
            return $response->withJson($this->result)
                ->withStatus(404);
        }

        $permission->setRole($role);
        $permission->setResource($resource);
        $permission->setMethod($method);

        if (!$permission->validate()) {
            $validation = array();
            foreach ($permission->getValidationFailures() as $failure) {
                $validation[] = array(
                    'field'=>$failure->getPropertyPath(),
                    'message' => $failure->getMessage()
                );
            }
            $this->result['validation'] = $validation;
            $this->result['message'] = 'validation error';

            return $response
                ->withJson($this->result)
                ->withStatus(400);
        } else {
            $permission->save();
            $this->result['data'] = $permission->toArray();
            return $response
                ->withHeader("ACL", "REVOKE")
                ->withJson($this->result);
        }
    }

    public function delete(Request $request, Response $response, $args) {
        $params = explode('/', $request->getAttribute('params'));
        $id = isset($params[1]) ? filter_var($params[1], FILTER_VALIDATE_INT) : null;
        if(!is_int($id)) {
            $this->result['message'] = 'Item "Id" is required!';
            return $response
                ->withJson($this->result)
                ->withStatus(412);
        }

        $permission = \Models\PermissionQuery::create()->findPk($id);
        if(!is_object($permission)) {
            $this->result['message'] = 'Item does not exists!';
            return $response
                ->withJson($this->result)
                ->withStatus(404);
        }

        $permission->delete();
        if($permission->isDeleted()){
            $this->result['message'] = 'Item deleted successfully!';
            return $response
                ->withHeader("ACL", "REVOKE")
                ->withJson($this->result);
        } else {
            $this->result['message'] = 'Item was not deleted!';
            return $response
                ->withJson($this->result)
                ->withStatus(422);
        }
    }
}