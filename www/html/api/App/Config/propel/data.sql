SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'guest'),
(2, 'superadmin'),
(3, 'admin');

INSERT INTO `users` (`id`, `email`, `password`, `name`) VALUES 
(NULL, 'admin@admin.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'Diego Luis Restrepo');

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
(1, 2);

INSERT INTO `resources` (`id`, `path`) VALUES
(1, '*'),
(2, '/'),
(3, '/login'),
(4, '/register'),
(5, '/resources'),
(6, '/permissions'),
(7, '/roles'),
(8, '/users'),
(9, '/categories'),
(10, '/products');

INSERT INTO `permissions` (`id`, `role_id`, `resource_id`, `method`) VALUES
(1, 2, 1, '*'),
(2, 1, 3, 'POST'),
(3, 1, 4, 'POST'),
(4, 4, 10, 'POST');
