
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- entities
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `entities`;

CREATE TABLE `entities`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- roles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unq_usr_email` (`email`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users_roles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users_roles`;

CREATE TABLE `users_roles`
(
    `user_id` INTEGER NOT NULL,
    `role_id` INTEGER NOT NULL,
    PRIMARY KEY (`user_id`,`role_id`),
    UNIQUE INDEX `unq_usrrol_key` (`user_id`, `role_id`),
    INDEX `fi__rol_fk` (`role_id`),
    CONSTRAINT `rol_usr_fk`
        FOREIGN KEY (`user_id`)
        REFERENCES `users` (`id`),
    CONSTRAINT `usr_rol_fk`
        FOREIGN KEY (`role_id`)
        REFERENCES `roles` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- resources
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unq_res_path` (`path`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- permissions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role_id` INTEGER NOT NULL,
    `resource_id` INTEGER NOT NULL,
    `method` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unq_per_key` (`role_id`, `resource_id`, `method`),
    INDEX `fi__res_fk` (`resource_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- categories
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unq_cat_name` (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- products
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `is_available` TINYINT(1) NOT NULL,
    `is_best_seller` TINYINT(1) NOT NULL,
    `price` INTEGER NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `image` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unq_pro_name` (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- products_categories
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `products_categories`;

CREATE TABLE `products_categories`
(
    `product_id` INTEGER NOT NULL,
    `category_id` INTEGER NOT NULL,
    PRIMARY KEY (`product_id`,`category_id`),
    UNIQUE INDEX `unq_procat_key` (`product_id`, `category_id`),
    INDEX `fi__pro_fk` (`category_id`),
    CONSTRAINT `pro_cat_fk`
        FOREIGN KEY (`product_id`)
        REFERENCES `products` (`id`),
    CONSTRAINT `cat_pro_fk`
        FOREIGN KEY (`category_id`)
        REFERENCES `categories` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
