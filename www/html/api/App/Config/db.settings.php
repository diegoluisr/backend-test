<?php

$dbname = getenv('DB_DATABASE');
$dbhost = getenv('DB_HOST');
$dbuser = getenv('DB_USER');
$dbpass = getenv('DB_PASS');

$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion('2.0.0-dev');
$serviceContainer->setAdapterClass('default', 'mysql');
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
$manager->setConfiguration(array (
  'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
  'dsn' => "mysql:host=$dbhost;dbname=$dbname",
  'user' => $dbuser,
  'password' => $dbpass,
  'attributes' =>
    array (
      'ATTR_EMULATE_PREPARES' => false,
      'ATTR_TIMEOUT' => 30,
    ),
    'settings' =>
    array (
      'charset' => 'utf8',
      'queries' =>
      array (
        'utf8' => 'SET NAMES utf8 COLLATE utf8_general_ci, COLLATION_CONNECTION = utf8_general_ci, COLLATION_DATABASE = utf8_general_ci, COLLATION_SERVER = utf8_general_ci',
      ),
    ),
    'model_paths' =>
    array (
      0 => 'src',
      1 => 'vendor',
    ),
  ));
$manager->setName('default');
$serviceContainer->setConnectionManager('default', $manager);
$serviceContainer->setDefaultDatasource('default');