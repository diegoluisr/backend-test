<?php
namespace Middlewares;

use SimpleAcl\Acl as SimpleAcl,
    SimpleAcl\Role as AclRole,
    SimpleAcl\Resource as AclResource,
    SimpleAcl\Rule as AclRule;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use \Propel\Runtime\ActiveQuery\Criteria;

/**
 * Middleware validate user access
 */
class ACL {

    const KEY = 'ACL';

    private $settings = [
        'folder' => './',
        'file' => 'data.bin'
    ];

    private $acl = null;

    public function __construct($settings = null) {
        if ($settings !== null) {
            $this->settings = array_merge($this->settings, $settings);
        }
        $this->acl = $this->getACL();
    }

    private function getACL() {
        if (file_exists($this->settings['folder'] . $this->settings['file'])) {
            $acl = unserialize(file_get_contents($this->settings['folder'] . $this->settings['file']));
        } else {
            $acl = new SimpleAcl();

            $roles = ['guest'];

            $roles_data = \Models\RoleQuery::create()
                ->find();

            foreach($roles_data as $role) {
                $roles[] = $role->getName();
            }

            $permissions = \Models\PermissionQuery::create()
                ->joinWith('Permission.Role')
                ->joinWith('Permission.Resource')
                ->find();

            foreach ($permissions as $permission) {
                $acl->addRule(
                    new AclRole($permission->getRole()->getName()),
                    new AclResource($permission->getResource()->getPath()),
                    $permission->getMethod(), true);
            }

            // Enable next line in production
            file_put_contents($this->settings['folder'] . $this->settings['file'], serialize($acl));
        }
        return $acl;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
        $roles = ['guest'];

        $jwt = $request->getAttribute("jwt");

        if($jwt != null && property_exists($jwt, 'roles')) {
            $roles = array_merge($roles, $jwt->roles);
        }
        // $user = $this->getUser($request);
        
        $method = $request->getMethod();

        $routeInfo = $request->getAttribute('routeInfo');

        $resource = '/';
        if(isset($routeInfo['2']['params'])) {
            // $params = $routeInfo['2']['params'];
            $params = explode('/', $routeInfo['2']['params']);
            $resource = '/'.$params[0];
        }

        $allowed = false;

        foreach ($roles as $role) {
            if ($this->acl->isAllowed($role, $resource, $method) || 
                $this->acl->isAllowed($role, $resource, '*') ||
                $this->acl->isAllowed($role, '*', $method) ||
                $this->acl->isAllowed($role, '*', '*')) {
                $allowed = true;
                break;
            }
        }

        if($allowed) {
            $response = $next($request, $response);

            $revoke = $response->getHeaderLine('ACL');
            if($revoke == "REVOKE") {
                if (file_exists($this->settings['folder'] . $this->settings['file'])) {
                    unlink($this->settings['folder'] . $this->settings['file']);
                } 
            }

            return $response;
        } else {
            $context = ['resource' => $resource, 'method' => $method, 'roles' => $roles];
            return $response->withJson(array('message' => 'Unauthorized (ACL)', 'data' => $context))->withStatus(401);
        }
    }
}