<?php
namespace Middlewares;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
* Middleware validate user access
*/
class Token {

    // https://jwt.io/

    const KEY = 'TOKEN';

    private $settings = [
        'name' => 'HTTP_TOKEN',
        'expire' => 'P1D',
    ];

    public function __construct($settings = null) {
        if ($settings !== null) {
            $this->settings = array_merge($this->settings, $settings);
        }
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {

        $ip = $request->getAttribute('ip_address');
        $token = isset($request->getHeader($this->settings['name'])[0]) ? $request->getHeader($this->settings['name'])[0] : null;
        $agent = isset($request->getHeader('HTTP_USER_AGENT')[0]) ? $request->getHeader('HTTP_USER_AGENT')[0] : '';

        if($token) {
            $access_token = \Models\AccessTokenQuery::create()
                ->filterByUserAgent($agent)
                ->filterByToken($token)
                ->filterByIp($ip)
                ->findOne();
            // var_dump($ip);

            if(is_object($access_token)) {

                // Revalidate access token
                $time = new \DateTime(date('Y-m-d H:i:s'));
                if(isset($this->settings['expire'])) {
                    $time->add(new \DateInterval($this->settings['expire']));
                } else {
                    $time->add(new \DateInterval($this->settings['P1D']));
                }
                $access_token->setExpiresAt($time->format('Y-m-d H:i:s'));
                $access_token->save();

                $request = $request->withAttribute('current_user', $access_token->getUser()->toArray());


                // var_dump($access_token->getUser()->toArray());
                // http://discourse.slimframework.com/t/how-can-i-pass-an-attribute-value-between-middlewares/528/1
            }
        }

        return $next($request, $response);
    }
}