<?php

namespace Utils;

use Psr\Http\Message\RequestInterface as Request;

/**
 * 
 * @author Diego Luis Restrepo <diegoluisr@gmail.com>
 * @version 1.0
 * @package Utils
 */
class SearchFilter {

    public static function filter(Request $request, string $model, Array $options = []) {
        $query = $request->getParam('Query');
        $limit = intval($request->getParam('Limit'));
        $page = intval($request->getParam('Page'));
        $order = $request->getParam('Order');
        $by = $request->getParam('By');

        $data_query = ("\\Models\\".$model."Query")::create();

        if(isset($options['SearchFields']) && isset($query) && !empty($query)) {
            $words = \Utils\Utils::getWords($query);
            if (isset($words)) {
                $i = 0;
                $conditions = [];
                foreach ($options['SearchFields'] as $field) {
                    foreach ($words as $word) {
                        $data_query->condition('cond' . $i, $model.'.'.$field . ' LIKE ?', '%' . $word . '%');
                        $conditions[] = 'cond' . $i;
                        $i++;
                    }
                }
                $data_query->where($conditions, 'or');
            }
        }

        $order = (isset($order) && !empty($order) && strtolower($order) == 'desc') ? 'desc' : 'asc';
        if(isset($options['OrderFields']) && isset($by) && !empty($by) && in_array($by, $options['OrderFields'])) {
            $orderBy = 'orderBy'.$by;
            $data_query->{$orderBy}($order);
        } else {
            $data_query->orderById($order);
        }

        $page = isset($page) && $page > 0 ? $page : 1;
        $limit = isset($limit) && $limit > 0 ? $limit : 20;
        $total = ceil($data_query->count() / $limit);
        $page = ($page > $total) ? $total : $page;

        return $data_query->paginate($page, $limit);
    }
}