<?php

namespace Utils;

/**
 * 
 * Clase que expone funciones estaticas relacionado con la subida de imagenes al servidor,
 * la modificacion de estas a otras dimensiones y obtener informacion de su proporcion ancho/alto
 * @author Diego Luis Restrepo <diegoluisr@gmail.com>
 * @version 1.0
 * @package Utils
 */
class File {

    /**
     * funcion estatica de apoyo a las subida de imagenes al servidor
     * @param string $storage ubicacion de la carpeta de almacenamiento
     * @param string $folder subcarpeta de almacenamiento
     * @return mixed 
     */
    public static function upload($storage, $folder = '/') {
        $types = array(
            'image/jpeg' => 'jpg',
            'image/jpg' => 'jpg',
            'image/png' => 'png',
        );

        if (isset($_FILES['uploaded_files']['tmp_name'])) {
            $file = $_FILES['uploaded_files'];
        } elseif (isset($_FILES['file']['tmp_name'])) {
            $file = $_FILES['file'];
        } elseif (isset($_FILES['tmp_name'])) {
            $file = $_FILES;
        }

        if (isset($file)) {
            if (array_key_exists($file['type'], $types)) {
                $tmpname = $file['tmp_name'];
                $filename = date('YmdHis') . '.' . \Utils\Utils::createToken() . '.' . $types[$file['type']];

                if (move_uploaded_file($tmpname, $storage . $folder . $filename)) {
                    return $filename;
                }
            }
        }
        return null;
    }

    /**
     * funcion estatica el redimensionamiento de las imagenes en el servidor
     * @param string $storage ubicacion de la carpeta de almacenamiento
     * @param string $folder subcarpeta de almacenamiento
     * @param string $filename nombre del archivo que se desea escalar
     * @param int $width ancho de la imagen resultante
     * @return boolean 
     */
    public static function scaleImage($storage, $folder, $filename, $width) {

        $ext = strrchr($storage . $folder . $filename, ".");
        ini_set('gd.jpeg_ignore_warning', true);
        switch ($ext) {
            case '.jpg':
                $originPicture = imagecreatefromjpeg($storage . $folder . $filename);
                break;
            case '.png':
                $originPicture = imagecreatefrompng($storage . $folder . $filename);
                break;
            default:
                return false;
        }

//        $src_img = @imagecreatefromjpeg($storage . $folder . $filename);
//        if (!$src_img) {
//            $src_img = LoadJPEG($storage . $folder . $filename);
//        }

        $originW = imagesx($originPicture);
        $originH = imagesy($originPicture);

        $targetW = $width;
        $targetH = intval($originH / $originW * $width);

        $targetPicture = imagecreatetruecolor($targetW, $targetH);

        // Adjust PNG Transparent
        imagecopyresized($targetPicture, $originPicture, 0, 0, 0, 0, $targetW, $targetH, $originW, $originH);

        if ($ext == '.jpg') {
            imagejpeg($targetPicture, $storage . $folder . $width . 'x/' . $filename, 85);
        } else {
            imagepng($targetPicture, $storage . $folder . $width . 'x/' . $filename, 8);
        }

        return true;
    }

    /**
     * funcion estatica obtiene la proporcion ancho/alto de una imagenes
     * @param string $storage ubicacion de la carpeta de almacenamiento
     * @param string $folder subcarpeta de almacenamiento
     * @param string $filename nombre del archivo que se desea escalar
     * @return mixed 
     */
    public static function getRatio($storage, $folder, $filename) {
        $ext = strrchr($storage . $folder . $filename, ".");

        switch ($ext) {
            case '.jpg':
                $picture = imagecreatefromjpeg($storage . $folder . $filename);
                return imagesy($picture) / imagesx($picture);
                break;
            case '.png':
                $picture = imagecreatefrompng($storage . $folder . $filename);
                return imagesy($picture) / imagesx($picture);
                break;
            default:
                return null;
        }
    }

}
