<?php
// http://docs.slimframework.com/
/* ----------------------------------------------------------------------
____________________ ________        ____._______________________________
\______   \______   \\_____  \      |    |\_   _____/\_   ___ \__    ___/
 |     ___/|       _/ /   |   \     |    | |    __)_ /    \  \/ |    |   
 |    |    |    |   \/    |    \/\__|    | |        \\     \____|    |   
 |____|    |____|_  /\_______  /\________|/_______  / \______  /|____|   
                  \/         \/                   \/         \/          
---------------------------------------------------------------------- */

require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

require 'App/Config/constants.php';
require 'App/Config/db.settings.php'; // DATABASE
$settings = require 'App/Config/settings.php';

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use \Propel\Runtime\Propel;
use \Propel\Runtime\Exception\PropelException;
use \Propel\Runtime\Connection\ConnectionManagerSingle;

$app = new \Slim\App($settings);

// --- NOTES ---
// https://github.com/oscarotero/psr7-middlewares
// http://help.slimframework.com/discussions/questions/8833-slim-3-pass-variables-from-middleware
// https://github.com/ziadoz/awesome-php#micro-frameworks
// http://www.slimframework.com/docs/features/caching.html

// https://github.com/K-Phoen/rulerz
$app->add(new Middlewares\ACL([
    'folder' => TMP_FOLDER
]));
$app->add(new \Slim\Middleware\JwtAuthentication([
    'secret' => JWT_SECRET,
    'attribute' => 'jwt',
    'algorithm' => ['HS256'],
    'path' => ['/'],
    'passthrough' => ['/login', '/register', '/password'],
    'error' => function ($request, $response, $arguments) {
        $data['message'] = 'Unauthorized (JWT)';
        return $response->withJson($data)->withStatus(401);
    }
]));
$app->add(new RKA\Middleware\IpAddress(false, []));
$app->add(new Middlewares\Payload());
$app->add(new \Tuupola\Middleware\Cors([
    'origin' => ['*', 'editor.swagger.io'],
    'methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    'headers.allow' => ['Authorization', 'If-Match', 'If-Unmodified-Since'],
    'headers.expose' => [],
    'credentials' => false,
    'cache' => 0,
]));
$app->add(new Middlewares\Trailing());


$app->map(['GET', 'POST', 'PUT', 'PATCH', 'DELETE'], '/[{params:.*}]', function ($request, $response, $args) use ($app) {
    $params = explode('/', $request->getAttribute('params'));

    if(count($params) == 1 && $params[0] == '') {
        $response = $response->withJson(array('api' => 'project'));
    } else {
        if(count($params) >= 1) {
            $ctlr_name = \Utils\Utils::Slug2CamelCase($params[0]);

            try{
                $class = new ReflectionClass('\\Controllers\\'.$ctlr_name . 'Controller');
                $instance = $class->newInstanceArgs();
                switch ($request->getMethod()) {
                    case 'GET':
                        $response = $instance->index($request, $response, $args);
                        break;
                    case 'POST':
                        $response = $instance->add($request, $response, $args);
                        break;
                    case 'PUT':
                        $response = $instance->edit($request, $response, $args);
                        break;
                    case 'PATCH':
                        $response = $instance->patch($request, $response, $args);
                        break;
                    case 'DELETE':
                        $response = $instance->delete($request, $response, $args);
                        break;
                    default:
                        $response = $response->withJson(array('message' => 'Unavailable method'));
                        break;
                }
            } catch (PropelException $pe) {
                $result = [
                    'error' => 'PropelException',
                    'message' => $pe->getMessage(),
                    'file' => $pe->getFile(),
                    'line' => $pe->getLine()
                ];
                $response = $response->withJson($result)->withStatus(400);
            } catch (\ReflectionException $re) {
                $result = [
                    'error' => 'ReflectionException',
                    'message' => $re->getMessage(),
                    'file' => $re->getFile(),
                    'line' => $re->getLine()
                ];
                $response = $response->withJson($result)->withStatus(404);
            }
        } else {
            $response = $response->withJson(array('message' => 'Unavailable number of params'));
        }
    }
    return $response;
});

$app->run();