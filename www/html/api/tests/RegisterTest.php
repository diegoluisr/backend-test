<?php

require_once('../vendor/autoload.php');
require_once('./Utils/CsvFileIterator.php');

class RegisterTest extends PHPUnit_Framework_TestCase {
    protected $client;

    protected function setUp() {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://localhost/api/'
        ]);
    }

    /**
     * @dataProvider additionProvider
     */
    public function testPost_Register($name, $email, $password, $responseStatusCode) {
        try {
            $response = $this->client->post('register', [
                'json' => [
                    'Name'    => $name,
                    'Email'    => $email,
                    'Password' => $password
                ]
            ]);
            $this->assertEquals($responseStatusCode, $response->getStatusCode());

        } catch (GuzzleHttp\Exception\ClientException $ce) {
            $this->assertEquals($responseStatusCode, $ce->getResponse()->getStatusCode());
        }
    }

    public function additionProvider() {
        return new CsvFileIterator('./datasources/register.csv'); 
    }
}