<?php

require_once('../vendor/autoload.php');
require_once('./Utils/CsvFileIterator.php');

class LoginTest extends PHPUnit_Framework_TestCase {
    protected $client;

    protected function setUp() {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://localhost/api/'
        ]);
    }

    public function testGet_Login() {
        try {
            $response = $this->client->get('login', ['http_errors' => true]);
        } catch (GuzzleHttp\Exception\ClientException $ce) {
            $this->assertEquals($ce->getResponse()->getStatusCode(), 401);
        }
    }

    /**
     * @dataProvider additionProvider
     */
    public function testPost_Login($email, $password, $responseStatusCode) {
        try {
            if(!empty($email)) {
                $response = $this->client->post('login', [
                    'json' => [
                        'Email'    => $email,
                        'Password' => $password
                    ]
                ]);
            } else {
                $response = $this->client->post('login');
            }
            $this->assertEquals($response->getStatusCode(), $responseStatusCode);
        } catch (GuzzleHttp\Exception\ClientException $ce) {
            $this->assertEquals($ce->getResponse()->getStatusCode(), $responseStatusCode);
        }
    }

    public function additionProvider() {
        return new CsvFileIterator('./datasources/login.csv'); 
    }
}