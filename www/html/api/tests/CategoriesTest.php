<?php

require_once('../vendor/autoload.php');
require_once('./Utils/CsvFileIterator.php');

class VehicleTest extends PHPUnit_Framework_TestCase {
    protected $client;
    protected $token;

    protected function setUp() {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://localhost/api/'
        ]);
    }

    /**
     * @dataProvider additionProvider
     */
    public function testPost_Categories($email, $password, $name, $responseStatusCode) {

        try {

            $login_request = $this->client->post('login', [
                'json' => [
                    'Email'    => $email,
                    'Password' => $password
                ],
                'http_errors' => false
            ]);
            $this->assertEquals($login_request->getStatusCode(), 200);

            $login_data = json_decode($login_request->getBody(), true);

            $this->assertTrue(isset($login_data['data']));
            if(isset($login_data['data'])) {
                $response = $this->client->request('POST', 'categories', [
                    'json' => [
                        'Name'    => $name
                    ],
                    'headers' => [
                        $login_data['data']['header'] => $login_data['data']['header'] . ' ' . $login_data['data']['credentials']
                    ],
                    'http_errors' => true
                ]);

                $this->assertEquals($response->getStatusCode(), $responseStatusCode);
            }
        } catch (GuzzleHttp\Exception\ClientException $ce) {
            $this->assertEquals($ce->getResponse()->getStatusCode(), 401);
        }
    }

    public function additionProvider() {
        return new CsvFileIterator('./datasources/categories.csv');
    }
}