#!/usr/bin/env bash

# https://gist.github.com/rrosiek/8190550

# Variables
DBNAME=api_db
DBUSER=root
DBPASSWD=toor
DBSCHEMAEXPORT=/vagrant/www/html/api/App/Config/propel/default.sql
DBDATAEXPORT=/vagrant/www/html/api/App/Config/propel/data.sql

# PHPDIR=/etc/php5
PHPDIR=/etc/php/7.0

echo -e "\n--- We are ready! ---\n"
echo -e "\n--- installing now... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install curl build-essential unzip > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
apt-get -y install mysql-server > /dev/null 2>&1


echo -e "\n--- Installing Apache ---\n"
apt-get -y install apache2 > /dev/null 2>&1

echo -e "\n--- Installing Apache PHP-specific packages ---\n"
#Ubuntu 14
#apt-get -y install php5 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-mysql php-apc > /dev/null 2>&1
#Ubuntu 16
apt-get -y install php libapache2-mod-php php-curl php-gd php-mcrypt php-mysql php-apcu php-xml php-mbstring > /dev/null 2>&1

echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite > /dev/null 2>&1

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- Creating symbolic link to use as new server root ---\n"
if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant/www /var/www
fi

echo -e "\n--- Enabling PHP error reporting ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" $PHPDIR/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" $PHPDIR/apache2/php.ini

echo -e "\n--- Installing PHPUnit ---\n"
# version ~4.8
# wget https://phar.phpunit.de/phpunit-old.phar > /dev/null 2>&1
# chmod +x phpunit-old.phar
# mv phpunit-old.phar /usr/local/bin/phpunit
# version ~5.7
wget https://phar.phpunit.de/phpunit.phar > /dev/null 2>&1
chmod +x phpunit.phar
mv phpunit.phar /usr/local/bin/phpunit

echo -e "\n--- Installing Composer for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Restarting Apache ---\n"
service apache2 restart > /dev/null 2>&1

echo -e "\n--- Configuring Project ---\n"
echo -e "- Installing composer dependencies \n"
if ! [ -f /var/www/api/composer.json ]; then
    cd /vagrant/www/html/api
    composer install --no-plugins --no-scripts > /dev/null 2>&1
fi

echo -e "- Create database \n"
mysql -u $DBUSER -p$DBPASSWD -e "CREATE DATABASE $DBNAME DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci" > /dev/null 2>&1
echo -e "- Importing database scheme and data \n"
mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBSCHEMAEXPORT > /dev/null 2>&1
mysql -u $DBUSER -p$DBPASSWD $DBNAME < $DBDATAEXPORT > /dev/null 2>&1
